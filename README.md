# 0list-persistence

## Prerequisites

- node 12+
- yarn

## Usage

```
yarn install
yarn start <db-address>
```

## License

0list is licensed under the terms of the MIT license.
