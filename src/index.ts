// @ts-nocheck
// @ts-ignore
import IPFS from 'ipfs'
// @ts-ignore
import WebRTCStar from 'libp2p-webrtc-star'
// @ts-ignore
import OrbitDB from 'orbit-db'
import Rtap from 'ramda/src/tap'
// @ts-ignore
import wrtc from 'wrtc'

if (process.argv.length < 3) {
  console.error('Please provide db address')
  process.exit(1)
}

const
  address = process.argv[ 2 ],
  options = {
    indexBy: 'id',
    create: false,
    type: 'keyvalue',
    overwrite: false,
  },
  config = {
    start: true,
    relay: {
      enabled: false, // enable relay dialer/listener (STOP)
      hop: {
        enabled: false, // make this node a relay (HOP)
      },
    },
    preload: {
      enabled: false,
    },
    repo: "./zlist",
    EXPERIMENTAL: { pubsub: true },
    config: {
      Bootstrap: [
        // "/dns4/ams-1.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLer265NRgSp2LA3dPaeykiS1J6DifTC88f5uVQKNAd",
        // "/dns4/lon-1.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLMeWqB7YGVLJN3pNLQpmmEk35v6wYtsMGLzSr5QBU3",
        // "/dns4/sfo-3.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLPppuBtQSGwKDZT2M73ULpjvfd3aZ6ha4oFGL1KrGM",
        // "/dns4/sgp-1.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLSafTMBsPKadTEgaXctDQVcqN88CNLHXMkTNwMKPnu",
        // "/dns4/nyc-1.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLueR4xBeUbY9WZ9xGUUxunbKWcrNFTDAadQJmocnWm",
        // "/dns4/nyc-2.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLV4Bbm51jM9C4gDYZQ9Cy3U6aXMJDAbzgu2fzaDs64",
        "/dns4/ipfs-ws.vps.revolunet.com/tcp/443/wss/ipfs/QmSEbJSiV8TXyaG9oBJRs2sJ5sttrNQJvbSeGe7Vt8ZBqt",
      ],
      Addresses: {
        Swarm: [
          //"/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star",
          // "/dns4/star-signal.cloud.ipfs.team/tcp/443/wss/p2p-webrtc-star",
          "/dns4/libp2p-rdv.vps.revolunet.com/tcp/443/wss/p2p-webrtc-star",
          //const addr = multiaddr('/ip4/188.166.203.82/tcp/20000/wss/p2p-webrtc-star/p2p/QmcgpsyWgH8Y8ajJz1Cu72KnS5uo2Aa2LpzU7kinSooo2a')
          //"/dns4/ipfs-ws.vps.revolunet.com/tcp/443/wss/ipfs/QmSEbJSiV8TXyaG9oBJRs2sJ5sttrNQJvbSeGe7Vt8ZBqt",
          //"/dns4/ws-star1.par.dwebops.pub/tcp/443/wss/p2p-websocket-star",
        ],
      },
    },
    libp2p: {
      modules: {
        transport: [ WebRTCStar ]
      },
      config: {
        peerDiscovery: {
          webRTCStar: { // <- note the lower-case w - see https://github.com/libp2p/js-libp2p/issues/576
            enabled: true
          }
        },
        transport: {
          WebRTCStar: { // <- note the upper-case w- see https://github.com/libp2p/js-libp2p/issues/576
            wrtc
          }
        }
      }
    }
  }

IPFS.create(config)
  .then((ipfs) =>
    ipfs.id()
      .then(Rtap((peerId) => console.log('ipfs started', peerId.id)))
      .then((peerId) => ({ ipfs, peerId })))
  // .then(Rtap(console.log))
  .then((state) =>
    OrbitDB.createInstance(state.ipfs)
      .then((orbit) => ({ ...state, orbit })))
  // .then(Rtap(console.log))
  .then((state) =>
    state.orbit.open(address, options)
      .then(Rtap((db) => console.log('orbitdb.opened', db.address.toString())))
      .then((db) => ({ ...state, db })))
  // .then(Rtap(console.log))
  .then((state) => {
    state.db.events.on('replicate', (address) => {
      console.log('db.events.replicate', address.toString())
      // return state.db.load()
    })
    state.db.events.on('replicated', (address) => {
      console.log('db.events.replicated', address.toString())
      return state.db.load()
    })
    state.db.events.on('write', (address) => {
      console.log('db.events.write', address.toString())
      return state.db.load()
    })
    return state.db.load()
  })
  .catch(console.error)
